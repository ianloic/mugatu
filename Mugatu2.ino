#define NUM_KEYS 8

#define MIDI

#define MIDI_CHANNEL 2


struct {
  int pin;
  int note;
  int callib;
  int on;
  char name;
  int last;
} key[NUM_KEYS] = {
  {  0, 72, 0, 0, 'C', 0 },
  { A9, 71, 0, 0, 'B', 0 },
  { A8, 69, 0, 0, 'A', 0 },
  { A5, 67, 0, 0, 'G', 0 },
  { A4, 65, 0, 0, 'F', 0 },
  { A3, 64, 0, 0, 'E', 0 },
  { A2, 62, 0, 0, 'D', 0 },
  { A1, 60, 0, 0, 'C', 0 },
};

#include <stdarg.h>
void debug(char *fmt, ... ){
        char tmp[128]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(tmp, 128, fmt, args);
        va_end (args);
//#ifndef MIDI
        Serial.print(tmp);
//#endif
}

void noteOn(int note, int velocity) {
#ifdef MIDI
  usbMIDI.sendNoteOn(note, velocity, MIDI_CHANNEL);
#endif
}

void noteOff(int note, int velocity) {
#ifdef MIDI
  usbMIDI.sendNoteOff  (note, velocity, MIDI_CHANNEL);
#endif
}



void setup() {
#ifdef MIDI
#else
    Serial.begin(38400);
#endif
// calibrate / mode button
  pinMode(11, OUTPUT);
  digitalWrite(11, LOW);
  pinMode(12, INPUT_PULLUP);
  
  // led
  pinMode(13, OUTPUT);
    callibrate();
}

// TODO better calibration
void callibrate() {
  long totals[NUM_KEYS];
#define SAMPLES 32
  debug("callibrating\n");
  digitalWrite(13, HIGH);
  for (int j=0; j<SAMPLES; j++) {
    for (int i=0; i<NUM_KEYS; i++) {
      totals[i] += touchRead(key[i].pin);
    }
    delay(10);
  }
  for (int i=0; i<NUM_KEYS; i++) {
    key[i].callib = totals[i]/SAMPLES;
  }
  digitalWrite(13, LOW);
  debug("calibrated");
}



void loop() {
  for (int i=0; i<NUM_KEYS; i++) {
    int value = touchRead(key[i].pin);
    // TODO better touch logic? debounce? etc
    if (value >= key[i].callib*1.2) {
      // on
      if (!key[i].on) {
        // noteon
        noteOn(key[i].note, 99);
        debug("noteon:  %c %4d %4d\n", key[i].name, value, key[i].callib);
        key[i].on = true;

      }
    } else if (value <= key[i].callib*1.1) {
      // off
      if (key[i].on) {
        // noteoff
        noteOff(key[i].note, 99);
        debug("noteoff: %c %4d %4d\n", key[i].name, value, key[i].callib);
        key[i].on = false;
      }
    }
  }
  delay(50);
}
